1. Apakah perbedaan antara JSON dan XML?
JSON dan XML dua-duanya digunakan untuk menyiapkan dan memberikan data yang diperlukan oleh HTML untuk menampilkan informasi yang sesuai. Namun, ada beberapa perbedaan antara JSON dan XML, diantaranya adalah:
a. JSOn mensupport array, sedangkan XML tidak.
b. JSON tidak menggunakan tag, sedangkan XML harus menggunakan tag
c. JSON tidak mensupport comment, sedangkan XML mensupport comment
d. Objek dari JSON memiliki sebuah tipe data, sedangkan data XML tidak
e. JSON hanya mensupport encoding UTF-8, sedangkan XML mensupport beberapa format encoding lain.

2. Apakah perbedaan antara HTML dan XML?
HTML adalah file yang akan diberikan kepada browser ketika server menerima request yang dapat dibaca dan diproses oleh browser untuk menampilkan sebuah page, sedangkan XML menyediakan data yang akan digunakan oleh HTML untuk menampilkan informasi yang sesuai terhadap request. 