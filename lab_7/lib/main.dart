import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 7',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Lab 7 PBP'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final nameController = TextEditingController();
  final secondController = TextEditingController();
  String name = "";
  String isi = "";
  String capital = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Text(
                "SAPA KAMU",
                style: TextStyle(fontSize: 36),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: TextFormField(
                keyboardType: TextInputType.phone,
                controller: nameController,
                decoration: InputDecoration(
                    hintText: "Masukkan namamu",
                    contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(32),
                    )),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: ElevatedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    setState(() {
                      if (nameController.text != "") {
                        String temp = nameController.text;
                        name = "Halo $temp!";
                      } else {
                        name = "Jangan dibiarkan kosong!";
                      }
                    });
                  }),
            ),
            Text(name),
            Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: TextFormField(
                controller: secondController,
                onChanged: (text) {
                  setState(() {
                    isi = secondController.text;
                    if (isi.toLowerCase() != isi) {
                      capital = "ADA HURUF BESAR";
                    } else {
                      capital = "";
                    }
                  });
                },
                keyboardType: TextInputType.phone,
                obscureText: true,
                decoration: InputDecoration(
                    hintText: "Masukkan huruf-huruf kecil",
                    contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(32),
                    )),
              ),
            ),
            Text(isi),
            Text(capital,
                style: TextStyle(
                  color: Colors.red,
                )),
          ],
        ),
      ),
    );
  }
}
