from django.urls import path
from .views import index

app_name = "lab-5"
urlpatterns = [
    path('', index, name='index'),
]
