from lab_1.models import Friend
from django import forms

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']
        error_messages = {'required' : 'Please enter every form'}
        name_attrs = {'type' : 'text', 'placeholder' : 'Friend name'}
        npm_attrs = {'type' : 'text', 'placeholder' : 'NPM'}
        dob_attrs = {'type' : 'text', 'placeholder' : 'Date of Birth'}
        name = forms.CharField(label='Nama', required=True, max_length=30)
        npm = forms.IntegerField(label='NPM', required=True)
        dob = forms.DateField(label='Date of Birth', required=True)