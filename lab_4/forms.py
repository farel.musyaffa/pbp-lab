from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'from_v', 'title', 'message']
        error_messages = {'required' : 'Please enter every form'}
        widgets = {
            "message": forms.Textarea
        }
        # to = forms.CharField(label='To', required=True, max_length=30)
        # from_v = forms.CharField(label='From', required=True, max_length=30)
        # title = forms.CharField(label='Title', required=True, max_length=30)
        # message = forms.CharField(widget=forms.Textarea)